

import java.util.ArrayList;


public class City {

	private String name;
	private int delay;

	public City(String cityName, int delayTime) {
		this.name = cityName;
		this.delay = delayTime;
	};

	public String getCityName() {
		return this.name;
	}

	public int getDelayTime() {
		return this.delay;
	}

	public void setCityName(String newName) {
		this.name = newName;
	}

	public void setDelayTime(int newDelay) {
		this.delay = newDelay;
	}

	

	public static void cityListPrinter(ArrayList<City> cityList) {
		for (int i = 0; i < cityList.size(); i++) {
			System.out.print("City: " + cityList.get(i).getCityName());
			System.out
					.println("      Delay: " + cityList.get(i).getDelayTime());
		}
	}

	



}
