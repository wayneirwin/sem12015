

import java.util.Comparator;

public class StateSpaceComparator implements Comparator<StateSpace> {
	
	@Override
    public int compare(StateSpace x, StateSpace y)
    {
        // Assume neither string is null. Real code should
        // probably be more robust
        // You could also just return x.length() - y.length(),
        // which would be more efficient.
        if (x.getHeuristic() < y.getHeuristic())
        {
            return -1;
        }
        if (x.getHeuristic() > y.getHeuristic())
        {
            return 1;
        }
        return 0;
    }
	

}
