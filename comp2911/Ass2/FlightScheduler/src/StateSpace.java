

import java.util.ArrayList;

public class StateSpace{
	


	
	
	private int currCost;
	private int heuristic;
	private ArrayList<City> cityList;
	private ArrayList<TravelLink> flightsLeft;
	private City currCity;
	
	public StateSpace(int currCostO,int heuristicCostO, ArrayList<TravelLink> flightsLeftO, ArrayList<City> cityListO, City currCityO) {
		this.currCost = currCostO;
		this.heuristic = heuristicCostO;
		this.flightsLeft = flightsLeftO;
		this.cityList = cityListO;	
		this.currCity = currCityO;
		
		
		
		
	}
	
	public int getHeuristic(){
		
		return this.heuristic;
	}
		
	
	public int getCost(){
		
		return this.currCost;
	}
	
	public int getCityListSize(){
		
		return this.cityList.size();
	}
	
	public City getCityListByIndex (int i){
		return this.cityList.get(i);
	}
	
	public int getFlightsLeftSize(){
		return this.flightsLeft.size();
		
	};
	
	public TravelLink getFligtsLeftByIndex(int i ){
		return flightsLeft.get(i);
		
	};
	

	public City getCurrCity(){
		
		return this.currCity;
	};
	public ArrayList<TravelLink> getAllRemainingFlights(){
		
		return this.flightsLeft;
	}
	

	
	public void StateSpacePrinter() {
		System.out.println("Cities in curr stateSpace: ");
		System.out.println("Currently at: " + this.getCurrCity().getCityName());
		System.out.print("Cities visitied: ");
		for (int i = 0; i < this.getCityListSize(); i++) {

			System.out.print(this.getCityListByIndex(i).getCityName() + ", ");
		}
		System.out.println();
		System.out.print("FLight Not Made: ");
		for (int i = 0; i < this.getFlightsLeftSize(); i++) {

			System.out.print(this.getFligtsLeftByIndex(i).GetOrigin()
					.getCityName()
					+ " - "
					+ this.getFligtsLeftByIndex(i).GetDestination()
							.getCityName() + ", ");
		}
		System.out.println();
		System.out.println("StateSpace Cost: " + this.getCost());

	}
	
	public void StateSpacePrinterToSpec() {
		System.out.println("cost = " + this.getCost());
		for(int i = 0; i < this.getCityListSize() - 1; i++){
			System.out.println("Flight " + this.getCityListByIndex(i).getCityName() + " to " + this.getCityListByIndex(i + 1).getCityName());
		}
		
		
	}
	
	
	

	
	

}
