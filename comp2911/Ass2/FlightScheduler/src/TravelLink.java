

import java.util.ArrayList;

public class TravelLink {
	private City placeA;
	private City placeB;
	private int timeTaken;
	
	public TravelLink (City placeA, City placeB, int timeTaken){
		this.placeA = placeA;
		this.placeB = placeB;
		this.timeTaken = timeTaken;
		
	}
	
	public City GetOrigin(){
		return this.placeA;
	}
	public City GetDestination(){
		return this.placeB;		
	}
	public int GetTimeTaken(){
		return this.timeTaken;
	}
	
	
	public void travelLinkPrinter(ArrayList<TravelLink> travelLinkList) {
		for (int i = 0; i < travelLinkList.size(); i++) {

			if (travelLinkList.get(i).GetTimeTaken() == 0) {
				System.out.println("Origin: "
						+ travelLinkList.get(i).GetOrigin().getCityName()
						+ "  Destination: "
						+ travelLinkList.get(i).GetDestination().getCityName()
						+ "   Time: NA");

			} else {
				System.out.println("Origin: "
						+ travelLinkList.get(i).GetOrigin().getCityName()
						+ "  Destination: "
						+ travelLinkList.get(i).GetDestination().getCityName()
						+ "   Time: " + travelLinkList.get(i).GetTimeTaken());
			}
		}

	}

}
