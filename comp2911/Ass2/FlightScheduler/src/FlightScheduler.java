import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.PriorityQueue;
import java.util.Scanner;


public class FlightScheduler {


	public static void main(String[] args) {
		ArrayList<City> cityList = new ArrayList<City>();
		ArrayList<TravelLink> travelTimes = new ArrayList<TravelLink>();
		Hashtable<String, Integer> quickTimes = new Hashtable<String, Integer>();
		ArrayList<TravelLink> flightsToBeMade = new ArrayList<TravelLink>();
		File file = new File(args[0]);
		try {

			Scanner sc = new Scanner(file);

			while (sc.hasNextLine()) {

				String i = sc.nextLine();
				String[] parts = i.split(" ");

				if (parts[0].equals("City")) {
					City curr = new City(parts[1], Integer.parseInt(parts[2]));
					cityList.add(curr);
				}
				if (parts[0].equals("Time")) {
					City origin = null;
					City destination = null;
					for (int m = 0; m < cityList.size(); m++) {
						if (cityList.get(m).getCityName().equals(parts[1])) {
							// Found the city
							origin = cityList.get(m);
						}

					}
					for (int m = 0; m < cityList.size(); m++) {
						if (cityList.get(m).getCityName().equals(parts[2])) {
							// Found the city
							destination = cityList.get(m);
						}

					}

					TravelLink curr1 = new TravelLink(origin, destination,
							Integer.parseInt(parts[3]));
					TravelLink curr2 = new TravelLink(destination, origin,
							Integer.parseInt(parts[3]));

					// Declartaion of travel time
					travelTimes.add(curr1);
					travelTimes.add(curr2);
					quickTimes.put(
							origin.getCityName() + destination.getCityName(),
							Integer.parseInt(parts[3]));
					quickTimes.put(
							destination.getCityName() + origin.getCityName(),
							Integer.parseInt(parts[3]));
					// decalre3d in both dir

				}
				if (parts[0].equals("Flight")) {
					// Decalring a flight that need to be made
					City origin = null;
					City destination = null;
					for (int m = 0; m < cityList.size(); m++) {
						if (cityList.get(m).getCityName().equals(parts[1])) {
							// Found the city
							origin = cityList.get(m);
						}

					}
					for (int m = 0; m < cityList.size(); m++) {
						if (cityList.get(m).getCityName().equals(parts[2])) {
							// Found the city
							destination = cityList.get(m);
						}

					}

					TravelLink curr1 = new TravelLink(origin, destination, 0);
					flightsToBeMade.add(curr1);

				}

			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		//System.out.println("File Read complete");
		// set hueristic
		aStarSearch(cityList, travelTimes, flightsToBeMade, quickTimes, new noHeuristic());
	}
	
	public static void aStarSearch(ArrayList<City> cityList,
			ArrayList<TravelLink> travelTimes,
			ArrayList<TravelLink> flightsToBeMade,
			Hashtable<String, Integer> quickTimes, noHeuristic noHeuristic
			) {
		IHeuristic huer = new noHeuristic();
		int master = 0; // expansion counter
		// Make sydney a stateSpace and add it to que by hand
		City Sydney = new City("Sydney", 0);
		ArrayList<City> sydneyList = new ArrayList<City>();
		sydneyList.add(Sydney); // Add sydney on the list of places visitged

		// StateSpace(int currCostO, ArrayList<TravelLink> flightsLeftO,
		// ArrayList<TravelLink> flightsMadeO, ArrayList<City> cityListO);
		StateSpace first = new StateSpace(0, 0, flightsToBeMade, sydneyList,
				Sydney);

		// Make Queue
		Comparator<StateSpace> comparator = new StateSpaceComparator();
		PriorityQueue<StateSpace> queue = new PriorityQueue<StateSpace>(10,
				comparator);
		// Add sydney
		queue.add(first);

		StateSpace curr = null;

		// now the fun starts!!!!!

		while (queue.size() != 0) {
			master++;
			// Pop elm of queue
			curr = queue.remove();

			// StateSpacePrinter(curr);
			// check if finished state
			if (isFinalStat(curr)) {
				// done
				break;

			}
			eachStateSpace: for (int i = 0; i < cityList.size(); i++) {

				// Add new state for everyCity except currCity

				City destinationCity = cityList.get(i);
				// Case to skip for comapring the same city
				if (curr.getCurrCity().getCityName()
						.equals(destinationCity.getCityName())) {
					// System.out.println("Skipping a-a");
					continue;
				}
				
				int travelTime = huer.determineHueristic(quickTimes.get(curr.getCurrCity().getCityName() + destinationCity.getCityName()), curr);
				ArrayList<TravelLink> flightsToBeMadeInState = new ArrayList<TravelLink>();
				for (int e = 0; e < curr.getFlightsLeftSize(); e++) {
					// Copies reaming flight to curr state
					flightsToBeMadeInState.add(curr.getFligtsLeftByIndex(e));

				}

				ArrayList<City> cityVisitedInState = new ArrayList<City>();
				for (int u = 0; u < curr.getCityListSize(); u++) {
					cityVisitedInState.add(curr.getCityListByIndex(u));
				}
				// Add curr City to cityList
				cityVisitedInState.add(destinationCity);
				for (int q = 0; q < flightsToBeMadeInState.size(); q++) {
					// Detects of flight just made was a forced one, if so ,
					// does list stuff

					if ((flightsToBeMadeInState.get(q).GetOrigin()
							.getCityName().equals(curr.getCurrCity()
							.getCityName()))
							&& (flightsToBeMadeInState.get(q).GetDestination()
									.getCityName().equals(destinationCity
									.getCityName()))) {
						// we just made this flight, rmove it from list
						flightsToBeMadeInState.remove(q);
						// Force to take flight, ignore other spacces
						StateSpace newStateSpace = new StateSpace(
								curr.getCost() + travelTime, curr.getCost()
										+ travelTime, flightsToBeMadeInState,
								cityVisitedInState, destinationCity);
						queue.add(newStateSpace);
						break eachStateSpace;

					}
					
					
				}
				// StateSpace(int currCostO, ArrayList<TravelLink> flightsLeftO,
				// ArrayList<TravelLink> flightsMadeO, ArrayList<City>
				// cityListO);
				// condition to never fly anywhere if it isnt the source or a flight left to be made
				for(int y = 0; y < flightsToBeMadeInState.size(); y++){
					if(flightsToBeMadeInState.get(y).GetOrigin().getCityName().equals(destinationCity.getCityName())){
						// IT ok to add this to the queue
						StateSpace newStateSpace = new StateSpace(curr.getCost()
								+ travelTime, curr.getCost() + travelTime,
								flightsToBeMadeInState, cityVisitedInState,
								destinationCity);
						queue.add(newStateSpace);
					}
					
				}
		
				// StateSpacePrinter(newStateSpace);
				// travelTime, );

			}

		}

//		System.out.println("A* EXITED");
//		curr.StateSpacePrinter();
		System.out.println(master + " nodes expanded");
		curr.StateSpacePrinterToSpec();
//		for (int m = 0; m < 15; m++){
//			StateSpace temp = queue.remove();
//			temp.StateSpacePrinterToSpec();
//			
//		}

	}
	
	public static Boolean isFinalStat(StateSpace currState) {
		// detects if all the flights are made
		if (currState.getFlightsLeftSize() == 0) {
			// All required flights made
			return true;

		} else {
			return false;
		}

	};
	
	

}
