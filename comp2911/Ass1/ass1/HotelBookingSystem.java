import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class HotelBookingSystem implements Comparable<HotelBookingSystem>{
	
	String HotelName;
	int RoomNumber;
	int RoomSize;
	int addOrder;
	ArrayList<DatePoint> Avaliability;
	
	public HotelBookingSystem (String NewHotelName, int NewRoomNumber, int NewRoomSize, int setAddOrder){
		HotelName = NewHotelName;
		RoomNumber = NewRoomNumber;
		RoomSize = NewRoomSize;
		addOrder = setAddOrder;
		Avaliability = new ArrayList<DatePoint>();
		ArrayList<String> HotelNames = new ArrayList<String>();	
	}
	
	
	   @Override
	    public int compareTo(HotelBookingSystem curr) {
	        //let's sort the employee based on id in ascending order
	        //returns a negative integer, zero, or a positive integer as this employee id
	        //is less than, equal to, or greater than the specified object.
	        return (this.addOrder - curr.addOrder);
	    }

	
	
	
	public static void main(String[] args) {
	HotelList masterList = new HotelList();
	ArrayList<String> ActiveHotelNames = new ArrayList<String>();
	int addOrder = 0;
    File file = new File(args[0]);   
    try {

        Scanner sc = new Scanner(file);

        while (sc.hasNextLine()) {
            String i = sc.nextLine();
            String[] parts = i.split(" ");

            
            
//            One the string is split on words, we can start doing some detection
            if (parts[0].equals("Hotel")){//            	This means its a state declaration
            	AddHotel(parts, masterList, ActiveHotelNames, addOrder);
            	addOrder++;
            
            	
            }
            if (parts[0].equals("Booking")){
//            	This means its a booking
//            	System.out.println("Booking Add");
            	AddBooking(parts, masterList, ActiveHotelNames);

            }
            if (parts[0].equals("Change")){
//            	This means its a boooking change
            	if (masterList.isValidID(Integer.parseInt(parts[1]))){
            	ChangeHotel(parts, masterList, ActiveHotelNames);
            	}
            	
            	else{
            		System.out.println("Change rejected");
            		
            	}
            	
            	
            }
            if (parts[0].equals("Cancel")){
//            	This means its a cancelation
            	if (masterList.isValidID(Integer.parseInt(parts[1]))){
            		masterList.delete(Integer.parseInt(parts[1]));
                	System.out.println("Cancel "+ Integer.parseInt(parts[1]));
                	}
                	
                	else{
                		System.out.println("Cancel rejected");
                		
                	}
            	
            	
            	
            }
            if (parts[0].equals("Print")){

//            	This means its an info dump
            	masterList.SpecHotelBookingSystemPrinter(parts[1]);
            	
            }
            else{
//            	IDK what to do, where fucked
            }

        }
        sc.close();
    } 
    catch (FileNotFoundException e) {
        e.printStackTrace();
    }
    //masterList.HotelBookingSystemPrinter();
   
  }

	public static int monthToInt(String month){
		
		Hashtable<String, Integer> months = new Hashtable<String, Integer>();
		months.put("Jan", 0);
		months.put("Feb", 1);
		months.put("Mar", 2);
		months.put("Apr", 3);
		months.put("May", 4);
		months.put("Jun", 5);
		months.put("Jul", 6);
		months.put("Aug", 7);
		months.put("Sep", 8);
		months.put("Oct", 9);
		months.put("Nov", 10);
		months.put("Dec", 11);
		
		return months.get(month);
		
		
	}
	
	
	public static int roomSizeToInt(String roomSize){
		
		Hashtable<String, Integer> size = new Hashtable<String, Integer>();
		size.put("single", 1);
		size.put("double", 2);
		size.put("triple", 3);
		
		return size.get(roomSize);
		
		
	}
	
	
	public static void ChangeHotel(String[] HotelSetter, HotelList masterList, ArrayList<String> ActiveHotelNames){
		// Detect the possible rooms it could be
		//System.out.println(Arrays.toString(HotelSetter));		
		int ID = Integer.parseInt(HotelSetter[1]);
		int nights = Integer.parseInt(HotelSetter[4]);		
		int month = monthToInt(HotelSetter[2]);
		int Day = Integer.parseInt(HotelSetter[3]);
		Calendar checkIn = Calendar.getInstance();
		Calendar checkOut = Calendar.getInstance();
		checkIn.set(2015, month, Day, 9, 0, 0);
		checkOut.set(2015, month, Day, 8, 59, 0);		
		checkOut.add(Calendar.DAY_OF_MONTH, nights);
		
		for(int w = 0; w < ActiveHotelNames.size(); w++){
			// loop through each hotel chain
			ArrayList<HotelBookingSystem> arrayForMultiRoom = new ArrayList<HotelBookingSystem>();
			int roomTotal = 0;
		
			
			for(int m = 0; m < HotelSetter.length - 5; m = m + 2){
				// Loop for mutple time of rooms
	
				
				int roomSize = roomSizeToInt(HotelSetter[5 + m]);
				
				roomTotal += Integer.parseInt(HotelSetter[6 + m]);
				int totalForThisRoomType = Integer.parseInt(HotelSetter[6 + m]);
				int thisRoomCounter = 0;
				//System.out.println("the number of rooms looking for is " + totalForThisRoomType);
	//			System.out.println("Room size:  " + roomSize + "  Number of rooms:  " + numberOfRooms);
			
				for(int i = 0; i < masterList.size(); i++ ){
					//Loop through hotel room list
					HotelBookingSystem curr = masterList.get(i);
					if (curr.RoomSize == roomSize && curr.HotelName.equals(ActiveHotelNames.get(w))){
						// Filters by size and makes sure the same location
		//				System.out.println("Potential Match: " + curr.RoomNumber + " at " + curr.HotelName);
						if(masterList.isAvailableForChange(curr, checkIn, checkOut, ID) == true){
							// Room free, add to list
							//System.out.println("Found A room ");
							arrayForMultiRoom.add(curr);
							thisRoomCounter++;
							
						}
						
						if((arrayForMultiRoom.size() == roomTotal) && (m +5  >= HotelSetter.length - 3 )){
							// We have filled the quota, 2nd cond means at the end fo the list
							System.out.print("Change " + ID + " " + ActiveHotelNames.get(w) + " ");
						
							for (int n = 0; n < arrayForMultiRoom.size(); n++){
								
								masterList.bookRoom(arrayForMultiRoom.get(n), checkIn, checkOut, ID);
								System.out.print(arrayForMultiRoom.get(n).RoomNumber + " ");
								
								
								
							}
							System.out.print("\n");
							return;
							
						}

					}
				
							
				}

				if(((roomTotal != arrayForMultiRoom.size()) && (w == ActiveHotelNames.size() -1)) || (totalForThisRoomType != Integer.parseInt(HotelSetter[6 + m]))){
					
					System.out.println("Change rejected");
					
					return;
					
					
				}
				if (totalForThisRoomType != thisRoomCounter){
					//System.out.println("Book Rejcted");
					
					break;
				}
			}

		
	}
		System.out.println("Change rejected");
	}
		
		
		
	public static void AddHotel(String[] HotelSetter, HotelList masterList, ArrayList<String> ActiveHotelNames, int newAddOrder){

		//System.out.println(Arrays.toString(HotelSetter));
		HotelBookingSystem newHotel = new HotelBookingSystem(HotelSetter[1], Integer.parseInt(HotelSetter[2]), Integer.parseInt(HotelSetter[3]), newAddOrder);
		masterList.append(newHotel);
		if(!ActiveHotelNames.contains(HotelSetter[1])){
			ActiveHotelNames.add(HotelSetter[1]);
		}


		
		
	}
	
	public static void AddBooking(String[] HotelSetter, HotelList masterList, ArrayList<String> ActiveHotelNames){
		// Detect the possible rooms it could be
		//System.out.println(Arrays.toString(HotelSetter));		
		int ID = Integer.parseInt(HotelSetter[1]);
		int nights = Integer.parseInt(HotelSetter[4]);		
		int month = monthToInt(HotelSetter[2]);
		int Day = Integer.parseInt(HotelSetter[3]);
		Calendar checkIn = Calendar.getInstance();
		Calendar checkOut = Calendar.getInstance();
		checkIn.set(2015, month, Day, 9, 0, 0);
		checkOut.set(2015, month, Day, 8, 59, 0);		
		checkOut.add(Calendar.DAY_OF_MONTH, nights);
		
		for(int w = 0; w < ActiveHotelNames.size(); w++){
			// loop through each hotel chain
			ArrayList<HotelBookingSystem> arrayForMultiRoom = new ArrayList<HotelBookingSystem>();
			int roomTotal = 0;
		
			
			for(int m = 0; m < HotelSetter.length - 5; m = m + 2){
				// Loop for mutple time of rooms
	
				
				int roomSize = roomSizeToInt(HotelSetter[5 + m]);
				
				roomTotal += Integer.parseInt(HotelSetter[6 + m]);
				int totalForThisRoomType = Integer.parseInt(HotelSetter[6 + m]);
				int thisRoomCounter = 0;
				//System.out.println("the number of rooms looking for is " + totalForThisRoomType);
	//			System.out.println("Room size:  " + roomSize + "  Number of rooms:  " + numberOfRooms);
			
				for(int i = 0; i < masterList.size(); i++ ){
					//Loop through hotel room list
					HotelBookingSystem curr = masterList.get(i);
					if (curr.RoomSize == roomSize && curr.HotelName.equals(ActiveHotelNames.get(w))){
						// Filters by size and makes sure the same location
		//				System.out.println("Potential Match: " + curr.RoomNumber + " at " + curr.HotelName);
						if(masterList.isAvailable(curr, checkIn, checkOut) == true){
							// Room free, add to list
							//System.out.println("Found A room ");
							arrayForMultiRoom.add(curr);
							thisRoomCounter++;
							
						}
						
						if((arrayForMultiRoom.size() == roomTotal) && (m +5  >= HotelSetter.length - 3 )){
							// We have filled the quota, 2nd cond means at the end fo the list

							
							Collections.sort(arrayForMultiRoom);
							
	
							
							System.out.print("Booking " + ID + " " + ActiveHotelNames.get(w) + " ");
							for (int n = 0; n < arrayForMultiRoom.size(); n++){
								
								masterList.bookRoom(arrayForMultiRoom.get(n), checkIn, checkOut, ID);
								System.out.print(arrayForMultiRoom.get(n).RoomNumber + " ");
								
								
								
							}
							System.out.print("\n");
							return;
							
						}

					}
				
							
				}

				if(((roomTotal != arrayForMultiRoom.size()) && (w == ActiveHotelNames.size() -1)) || (totalForThisRoomType != Integer.parseInt(HotelSetter[6 + m]))){
					
					System.out.println("Book Rejcted");
					
					return;
					
					
				}
				if (totalForThisRoomType != thisRoomCounter){
					//System.out.println("Book Rejcted");
					
					break;
				}
			}

		
	}
		System.out.println("Booking Rejected");
	}
		
		
		
		



		
		
	

	

}



