
import java.util.Calendar;

public class DatePoint implements Comparable<DatePoint>{
	
	Calendar checkIn;
	Calendar checkOut;
	int BookingID;
	
	public DatePoint(Calendar checkInSetter, Calendar checkOutSetter, int ID){

		checkIn = checkInSetter;
		checkOut = checkOutSetter;
		BookingID = ID;
	}
	
	   @Override
	    public int compareTo(DatePoint curr) {
	        //let's sort the employee based on id in ascending order
	        //returns a negative integer, zero, or a positive integer as this employee id
	        //is less than, equal to, or greater than the specified object.
		   
		   if(this.checkIn.after(curr.checkIn)){
			   return 1;
		   }
		   else{
			   return -1;
		   }
	    }

	
	
	public void SetCheckIn(Calendar checkInSetter){
		
		checkIn = checkInSetter;
	};
	
	public void SetCheckOut(Calendar checkOutSetter){
		
		checkOut = checkOutSetter;
	};
	
	public Calendar GetCheckIn(){
		return checkIn;		
	}
	
	public Calendar GetCheckOut(){
		return checkOut;		
	}
	
	public int GetID(){
		return BookingID;		
	}

}
