import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Hashtable;

public class HotelList {
	ArrayList<HotelBookingSystem> master = new ArrayList<HotelBookingSystem>();
	
	
	public void append(HotelBookingSystem NewHotelBookingSystem){
		
		master.add(NewHotelBookingSystem);
	}	
	
	public int size(){
		
		return master.size();
	}
	
	public HotelBookingSystem get(int index){
		return master.get(index);
		
	}
	
	public boolean isAvailable(HotelBookingSystem desieredLocation, Calendar CheckIn, Calendar CheckOut){
	if(desieredLocation.Avaliability.isEmpty()){
		// This means that there are no bookings for it
		// Any entry is allowed
		return true;
	}
		// There are booking need to check there is no overlap
		for (int i = 0; i < desieredLocation.Avaliability.size(); i++ ){
			if ((CheckIn.before(desieredLocation.Avaliability.get(i).GetCheckIn()) && CheckOut.before(desieredLocation.Avaliability.get(i).GetCheckIn())) || 
					(CheckIn.after(desieredLocation.Avaliability.get(i).GetCheckOut()) && CheckOut.after(desieredLocation.Avaliability.get(i).GetCheckOut())) ){
					// Booking Can be made
				
				
				}
				
			else{
				return false;
			}
			
		}
		
		
	
		//System.out.println("Found A Match");
	return true;
		
	}
	
	public void bookRoom(HotelBookingSystem desieredLocation, Calendar CheckIn, Calendar CheckOut, int ID){
		DatePoint currDP =  new DatePoint(CheckIn, CheckOut, ID);
		desieredLocation.Avaliability.add(currDP);
		
		
}
		
	
	public void delete(int ID){
		for(int i = 0; i < master.size(); i++){
			
			HotelBookingSystem curr = master.get(i);
			for(int m = 0; m < curr.Avaliability.size(); m++){
				if(curr.Avaliability.get(m).GetID() == ID){
					// Found ID to del
					curr.Avaliability.remove(m);
					
					
				}
				
			}
			
		}
		
	}
	
	public boolean isAvailableForChange(HotelBookingSystem desieredLocation, Calendar CheckIn, Calendar CheckOut, int ID){
		if(desieredLocation.Avaliability.isEmpty()){
			// This means that there are no bookings for it
			// Any entry is allowed
			return true;
		}
			// There are booking need to check there is no overlap
			for (int i = 0; i < desieredLocation.Avaliability.size(); i++ ){
				if ((CheckIn.before(desieredLocation.Avaliability.get(i).GetCheckIn()) && CheckOut.before(desieredLocation.Avaliability.get(i).GetCheckIn())) || 
						(CheckIn.after(desieredLocation.Avaliability.get(i).GetCheckOut()) && CheckOut.after(desieredLocation.Avaliability.get(i).GetCheckOut())) || 
						(desieredLocation.Avaliability.get(i).GetID() == ID) ){
						// Booking Can be made
					
					
					}
					
				else{
					return false;
				}
				
			}
			
			
		
			//System.out.println("Match Possible for change");
		return true;
			
		}

	
	public void HotelBookingSystemPrinter(){
		System.out.println("\n\n\n -------------HotelBookingSystem Logs ------------\n");
		for(int i = 0; i < master.size(); i++){
			HotelBookingSystem curr = master.get(i);
			System.out.println(curr.HotelName + " " + curr.RoomNumber + " " + curr.RoomSize);
			for (int m = 0; m < curr.Avaliability.size(); m++){
				Calendar temp1 = curr.Avaliability.get(m).GetCheckIn();
				Calendar temp2 = curr.Avaliability.get(m).GetCheckOut();
				
				System.out.println("Booking ID: " + curr.Avaliability.get(m).GetID());
				System.out.println("Check In: " + temp1.getTime());
				System.out.println("Check Out: " + temp2.getTime());
				
				
			}
			System.out.println("\n");
			
		}
		
	}
	
 	public void SpecHotelBookingSystemPrinter(String HotelBookingSystem){
 		
 		Hashtable<Integer, String> months = new Hashtable<Integer, String>();
 		months.put(0, "Jan");
 		months.put(1, "Feb");
 		months.put(2, "Mar");
 		months.put(3, "Apr");
 		months.put(4, "May");
 		months.put(5, "Jun");
 		months.put(6, "Jul");
 		months.put(7, "Aug");
 		months.put(8, "Sep");
 		months.put(9, "Oct");
 		months.put(10, "Nov");
 		months.put(11, "Dec");
 		
 		


for(int i = 0; i < master.size(); i++){
	HotelBookingSystem curr = master.get(i);
	if (curr.HotelName.equals(HotelBookingSystem)){
	System.out.print(curr.HotelName + " " + curr.RoomNumber + " ");
	for (int m = 0; m < curr.Avaliability.size(); m++){
		Collections.sort(curr.Avaliability);
		int days = curr.Avaliability.get(m).GetCheckOut().get(Calendar.DAY_OF_YEAR) - curr.Avaliability.get(m).GetCheckIn().get(Calendar.DAY_OF_YEAR);
		System.out.print(months.get(curr.Avaliability.get(m).GetCheckIn().get(Calendar.MONTH)) + " " + curr.Avaliability.get(m).GetCheckIn().get(Calendar.DAY_OF_MONTH) + " " + days + " ");


		
		
	}
	System.out.print("\n");
	}
	
}

}
 	
 	public boolean isValidID(int ID){
 		
 		for(int i = 0; i < master.size(); i++){
 			ArrayList<DatePoint> curr = master.get(i).Avaliability;
 			for(int m = 0; m < curr.size(); m++){
 				
 				if (curr.get(m).BookingID == ID){
 					
 					return true;
 				}
 				
 				
 			}
 			
 			
 		}
 		
 		
 		return false;
 	}


		
}

