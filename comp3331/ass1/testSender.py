#!/usr/bin/python
import sys
import socket

UDP_IP = "127.0.0.1"
UDP_PORT = sys.argv[1]
MESSAGE = sys.argv[2]



sock = socket.socket(socket.AF_INET, # Internet
                     int(socket.SOCK_DGRAM)) # UDP
sock.sendto(MESSAGE, (UDP_IP, int(UDP_PORT)))
