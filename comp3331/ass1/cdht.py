#!/usr/bin/python
import sys
import socket
import time
import select
#--------SET GLOBAL VARS----------#


UDP_IP = "127.0.0.1"
myID = ""
succ1 = ""
succ2 = ""
s = ""
r = ""

# read command line args
def argReader():
    global myID
    global succ1
    global succ2
    myID = sys.argv[1]
    succ1 = sys.argv[2]
    succ2 = sys.argv[3]
    return

# bind ports to process
def openPorts():
    global myID
    global r
    global s
    r = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Made Socket for receving
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # Made Socket for sending
    r.bind((UDP_IP, 5000 + int(myID)))
    r.setblocking(0)
    print "bound to socket: " + str((5000 + int(myID)))
    return r,s


# Check if we have the file
def requestChecker(val, source):
    global myID
    global succ1
    global succ2
    #print val
    hashedValue = hashFunction(val[0], val[1], val[2], val[3])
    # First check of you are the closest peer, if so then it is your file
    if int(hashedValue) >= int(myID) and ((int(hashedValue) < int(succ1)) or (int(myID) > int(succ1))):
        #print "MY FILE"
        if source == myID:
            print "File " + val + " is here."
            return
        else:
            print "File " + val + " is here."
            print "A response message, destined for peer" + source + ", has been sent."
            fireFileFound(val, source)

    else:
        print "File " + val +  " is not stored here."
        print "File request message has been forwarded to my successor."
        fireFileRequest(val, source)


# find hash value of file
def hashFunction(w, x, y, z):
    firstVal = 0 + int(w)
    secondVal = 0 +int(x)
    thirdVal = 0 + int(y)
    fourthVal = 0 + int(z)
    totalVals = [str(firstVal), str(secondVal), str(thirdVal), str(fourthVal)]
    strTemp = ""
    stringTotalVal = strTemp.join((totalVals))
    return (int(stringTotalVal)) % 256

# Send file request
def fireFileRequest(val, source):
    global s
    global succ1
    global myID
    # FR-<VAL>-<SOURCE>
    #print "firing file request"
    #print "sending payload: FR-" + val + "-" + source  + " to " + succ1
    s.sendto("FReq" + "-" + val + "-" + source, (UDP_IP, 5000 + int(succ1)))

# Send file found request
def fireFileFound(val, source):
    global s
    global myID
    s.sendto("FFound" + "-" + val + "-" + myID, (UDP_IP, 5000 + int(source)))

# Unpack file found request
def recieveFileFound(val, destination):
    print "Received a response message from peer " + destination + ", which has the file " + val +  " ."
# Send ping
def firePing(s, myID, toID):
    s.sendto("P-" + myID, (UDP_IP, 5000 + int(toID)))

# Unpack ping request
def firePingRes(s, myID, toID):
    s.sendto("R-" + myID, (UDP_IP, 5000 + int(toID)))

# Send quit signal
def sendQuit(oriSucc1, oriSucc2, killedID):
    global s
    global succ1
    global myID
    s.sendto("QUIT-" + oriSucc1 + "-" + oriSucc2 + "-" + killedID, (UDP_IP, 5000 + int(succ1)))
    if killedID == myID:
        print "Peer " + myID + " will depart from the network."
        sys.exit()


# Unpack and handle quit request
def recieveQuit(oriSucc1, oriSucc2, killedID):
    global myID
    global succ1
    global succ2
    # print "received quit. ID: " + killedID + " -- " + oriSucc1 + "-- " + oriSucc2 + "myID " + myID
    # print " my details: " + myID + " -- " + succ1 + " -- " + succ2
    if str(myID) == str(killedID):
        # print "found kill spot"
        sys.exit()
        return
    elif str(succ2) == str(killedID):
        # print "chaning succ2 to " + oriSucc1
        succ2 = oriSucc1
        print "My second successor is now peer " + succ2
        sendQuit(oriSucc1, oriSucc2, killedID)
    elif str(succ1) == str(killedID):
        # print "chaning succ1 to " + oriSucc1
        succ1 = oriSucc1
        print "My first successor is now peer " + succ1
        succ2 = oriSucc2
        print "My second successor is now peer " + succ2
        #sendQuit(oriSucc1, oriSucc2, killedID)
    else:
        # print "passing kill message"
        sendQuit(oriSucc1, oriSucc2, killedID)


# Handle all incomeing messagges are route to relevant functions
def recieveMessage(r, s, myID):
    data, addr = r.recvfrom(1024)  # buffer size is 1024 bytes
    splitData = data.split("-")
    if splitData[0] == "P":
        print "A ping request message was received from Peer " + splitData[1] + "."
        firePingRes(s, myID, splitData[1])
    elif splitData[0] == "R":
        print "A ping response message was received from Peer " + splitData[1] + "."
    if splitData[0] == "FReq":
        requestChecker(splitData[1], splitData[2])
    elif splitData[0] == "FFound":
        recieveFileFound(splitData[1], splitData[2])
    elif splitData[0] == "QUIT":
        recieveQuit(splitData[1], splitData[2], splitData[3])


def main():
    global myID
    global succ1
    global succ2
    argReader()
    openPorts()
    while True:
        # print "My succ1: " + succ1 + " succ2: " + succ2 + " and i am: " + myID
        # If there's input ready, do something, else do something
        # else. Note timeout is zero so select won't block at all.
        if select.select([sys.stdin,],[],[],0.0)[0]:
            # print "Have data!"
            line = sys.stdin.readline()
            splitLine = line.split("-")
            if str(splitLine[0]).rstrip() == "request":
                requestChecker(str(splitLine[1]).rstrip(), myID)
            if str(splitLine[0]).rstrip() == "quit":
                sendQuit(succ1, succ2, myID)
        read, write, error = select.select([r], [], [], 1)
        if read != []:
            # TIME TO READ
            recieveMessage(r, s, myID)
        if int(time.time()) % 20 == 0:
            # Ping Succ 1
            firePing(s, myID, succ1)
            # Ping Succ 2
            firePing(s, myID, succ2)
            time.sleep(1.0)
        # time.sleep(4.0)
if __name__ == "__main__":
    main()
