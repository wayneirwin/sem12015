{-# LANGUAGE GADTs #-}

module Solver where

import Formula


-- Evaluating terms
-- ----------------

eval :: Term t -> t
eval (Con t) = t
eval (And p q) =  (eval p) && (eval q)
eval (Or p q) = (eval p) || (eval q)
eval (Smaller p q) = (eval p) < (eval q)
eval (Plus p q) = (eval p) + (eval q)

-- Checking formulas
-- -----------------

satisfiable :: Formula ts -> Bool
satisfiable (Body t) = eval t
satisfiable (Forall [] form) = True -- empty list and forumla
satisfiable (Forall xs form) = any (satisfiable . form . Con) xs -- remember only one has to be right true for a TRUE output


-- otuput
--[(1,()),(2,()),(3,()),(4,()),(5,()),(6,()),(7,()),(8,()),(9,()),(10,())]



solutions :: Formula ts -> [ts]
solutions (Body term) = [() | eval term] -- base case
solutions (Forall xs form) = [ (x, ys) | x <- xs, ys <- solutions (form (Con x)) ]
