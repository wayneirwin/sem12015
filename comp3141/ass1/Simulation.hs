module Simulation (moveParticle, accelerate, advanceWorld) where

import World
import Physics

-- Move a particle according to its velocity for the given number of (simulated) seconds.
-- force :: Particle -> Particle -> Accel
moveParticle :: Float -> Particle -> Particle
moveParticle 0 p = p
moveParticle x (Particle m (px, py) (vx, vy)) = Particle m (vx*x + px, vy*x + py) (vx, vy)


-- Accelerate a particle in dependence on the gravitational force excerted by all other particles for
-- the given number of (simulated) seconds.
--
accelerate :: Float -> [Particle] -> [Particle]
accelerate t x = calAccleration 0 t x

-- Progressing the world state
--
advanceWorld :: unused -> Float -> World -> World
advanceWorld _ 0 (World a b c d) = World a b c d
advanceWorld _ s (World a b c d) = World a b c (moveRecursive (s*c) (accelerate (s*c) d))




-- index timeValue AprticleaList
calAccleration :: Int -> Float -> [Particle] -> [Particle]
calAccleration _ _ [] = []
calAccleration index f p@(x:xs)
  | index == length p = []
  | otherwise = [(rucursiveCal index f (p !! index) (x:xs))] ++ (calAccleration  (index + 1) f p)

-- runs through the list for each particle
rucursiveCal :: Int -> Float-> Particle -> [Particle] -> Particle
rucursiveCal _ _ x [] = x -- No more particles to add
rucursiveCal index t (Particle m (px, py) (vx, vy)) (p:ps)  = rucursiveCal index t (Particle m (px, py) (((fst (force (Particle m (px, py) (vx, vy)) p))*t + vx, (snd (force (Particle m (px, py) (vx, vy)) p))*t + vy))) ps


moveRecursive :: Float -> [Particle] -> [Particle]
moveRecursive _ [] = []
moveRecursive f (x:xs) = [(moveParticle f x)] ++ (moveRecursive f xs)
