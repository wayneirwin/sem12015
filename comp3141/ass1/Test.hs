module Test where

import Test.QuickCheck
import Control.Monad      (when)
import System.Environment (getArgs)
import System.Exit        (exitFailure)
import Graphics.Gloss

import World
import Visualise
import Physics
import Simulation
import TestSupport

--prop_EnergyConservation :: World -> Bool
prop_EnergyConservation (World a b c d)
  | oldWorld == 0 = True
  | otherwise = realToFrac(abs((newWorld - oldWorld) / (newWorld))) < epsilon
  where
    newWorld = (worldEnergy (advanceWorld 0 epsilon (World a b c d)))
    oldWorld = (worldEnergy (World a b c d))
