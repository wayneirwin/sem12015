import Graphics.Gloss

main = animate (InWindow "Exercise One" (300, 300) (10, 10)) black (animation . round)

-- Only alter the following three definitions.

picture1 = Color red (rectangleWire  100 50)

scaledPicture :: Float -> Picture
scaledPicture 0  = Blank
scaledPicture n = scale n n picture1
scaledPicture timeS = picture1

animation 0 = Blank
animation n = pictures[scaledPicture (fromIntegral n), animation ((fromIntegral n) -1)]
