module Ex04 where

import Data.Char
import System.Environment
import System.IO

capStr :: String -> String
capStr [] = [] --Base Case
capStr s@(x:xs) = [toUpper x] ++ (capStr xs) -- call recurseively
adder :: [String] -> Integer
adder [] = 0 -- Base case
adder s@(x:xs) = (read x :: Integer) + (adder xs)




capitalise :: FilePath -> FilePath -> IO ()
capitalise inn out = do
    contents <- readFile inn
    let upgarde = capStr contents
    writeFile out upgarde



sumFile :: IO ()
sumFile = do
    args <- getArgs
    inArg <- return (args !! 0)
    outArg <- return (args !! 1)
    inn <- readFile inArg
    let inputLines = lines inn
    let converted = adder inputLines
    writeFile outArg $ show converted
