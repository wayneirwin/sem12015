module Ex09 (BinaryTree(..), treeMap, concTreeMap) where

import Control.Concurrent
import System.Random
import Control.Applicative
import System.Environment

data BinaryTree a = Leaf | Branch a (BinaryTree a) (BinaryTree a) deriving (Read, Show, Eq)

treeMap :: (a -> b) -> BinaryTree a -> BinaryTree b
treeMap f Leaf = Leaf
treeMap f (Branch v l r) = let v' = f v
                               l' = treeMap f l
                               r' = treeMap f r
                           in
                           Branch v' l' r'




concTreeMap :: (a -> b) -> BinaryTree a -> IO (BinaryTree b)
-- concTreeMap function Leaf = return Leaf
-- concTreeMap function (Branch v l r) = do let v' = (function v)
--                                          l' <- (concTreeMap function l)
--                                          r' <- (concTreeMap function r)
--                                          return (Branch v' l' r')


concTreeMap function Leaf = return Leaf
concTreeMap function (Branch v l r) = do let v' = (function v)
                                         l' <- newEmptyMVar
                                         r' <- newEmptyMVar
                                         forkIO $ do
                                           putMVar l' (concTreeMap function l)
                                         forkIO $ do
                                           putMVar r' (concTreeMap function r)
                                         l'' <- takeMVar l'
                                         r'' <- takeMVar r'
                                         l3p <- l''
                                         r3p <- r''
                                         v' `seq` l' `seq` r' `seq` (return (Branch v' l3p r3p))
