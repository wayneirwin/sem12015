module Ex03 (
  BinaryTree(..), isBST, insertBST, searchTrees,
  prop_insert_on_empty_tree, prop_insert_preserves_bst, prop_insert_adds_element, prop_insert_does_not_change_other_elements,
  prop_insert_duplicate_check,
  prop_delete_detect_error, prop_delete_preserves_bst, prop_delete_removes_element, prop_delete_does_not_change_other_elements,
  height, size
) where

import Test.QuickCheck
import Data.List(sort, nub)

data BinaryTree = Branch Integer BinaryTree BinaryTree
                | Leaf
                deriving (Show, Ord, Eq)

isBST :: BinaryTree -> Bool
isBST Leaf = True
isBST (Branch v l r) = allTree (< v) l && allTree (>= v) r && isBST l && isBST r
  where allTree :: (Integer -> Bool) -> BinaryTree -> Bool
        allTree f (Branch v l r) = f v && allTree f l && allTree f r
        allTree f (Leaf) = True

insertBST :: Integer -> BinaryTree -> BinaryTree
insertBST i Leaf = Branch i Leaf Leaf
insertBST i (Branch v l r)
  | i < v = Branch v (insertBST i l) r
  | otherwise = Branch v l (insertBST i r)

searchTrees :: Gen BinaryTree
searchTrees = sized searchTrees'
  where
   searchTrees' 0 = return Leaf
   searchTrees' n = do
      v <- (arbitrary :: Gen Integer)
      fmap (insertBST v) (searchTrees' $ n - 1)


-- checkBST :: Integer -> BinaryTree -> Bool
-- checkBST x Leaf = False
-- checkBST x (Branch v l r) = (x == v)
-- checkBST x (Branch v l r)
--   | x < v = checkBST x l
--   | otherwise = checkBST x r

mysteryPred :: Integer -> BinaryTree -> Bool
mysteryPred _ Leaf = False
mysteryPred i (Branch v l r) =
        if i == v
          then True
        else if i < v
          then mysteryPred i l
        else mysteryPred i r



numOfElms :: Integer -> BinaryTree -> Integer
numOfElms _ Leaf = 0
numOfElms x (Branch v l r )
  | x == v = 1 + (numOfElms x l) + (numOfElms x r)
  |otherwise = (numOfElms x l) + (numOfElms x r)

incorrectDelete :: Integer -> BinaryTree -> BinaryTree
incorrectDelete i (Leaf) = Leaf
incorrectDelete i (Branch v l r) = Leaf

--------------

prop_searchTrees = forAll searchTrees isBST

prop_insert_on_empty_tree insertFunction integer = (insertFunction integer Leaf) == Branch integer Leaf Leaf

prop_insert_preserves_bst insertFunction integer = forAll searchTrees $ \tree -> isBST(insertFunction integer tree) == True


prop_insert_adds_element insertFunction integer = forAll searchTrees $ \tree -> numOfElms integer tree == numOfElms integer (insertFunction integer tree) - 1


prop_insert_does_not_change_other_elements insertFunction integer newInteger =
  if integer == newInteger
    then forAll searchTrees $ \tree -> numOfElms integer tree == numOfElms integer (insertFunction newInteger tree) - 1
  else forAll searchTrees $ \tree -> numOfElms integer tree == numOfElms integer (insertFunction newInteger tree)




prop_insert_duplicate_check insertFunction integer = forAll searchTrees $ \tree -> size tree == size(insertFunction integer tree) - 1

------------

prop_delete_detect_error deleteFunction i1 i2 =
  i1 /= i2 ==>
    deleteFunction i1 (Branch i2 Leaf Leaf) == Branch i2 Leaf Leaf

prop_delete_preserves_bst deleteFunction integer = forAll searchTrees $ \tree -> isBST(deleteFunction integer tree)

prop_delete_removes_element deleteFunction integer = forAll searchTrees $ \tree ->numOfElms integer tree == numOfElms integer (deleteFunction integer tree) - 1

prop_delete_does_not_change_other_elements deleteFunction integer newInteger = forAll searchTrees $ \tree -> mysteryPred newInteger tree == mysteryPred newInteger (deleteFunction integer tree)


----------------

height :: BinaryTree -> Integer
height Leaf = 0
height (Branch v l r ) = 1 + max(height l) (height r)

size :: BinaryTree -> Integer
size Leaf = 0
size (Branch v l r ) = 1 + (size l) + (size r)
