package FlightSystem;

public class TravelLink {
	private City placeA;
	private City placeB;
	private int timeTaken;
	
	public TravelLink (City placeA, City placeB, int timeTaken){
		this.placeA = placeA;
		this.placeB = placeB;
		this.timeTaken = timeTaken;
		
	}
	
	public City GetOrigin(){
		return this.placeA;
	}
	public City GetDestination(){
		return this.placeB;		
	}
	public int GetTimeTaken(){
		return this.timeTaken;
	}

}
